--
-- PostgreSQL database dump
--

-- Dumped from database version 11.4
-- Dumped by pg_dump version 11.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: gender; Type: TYPE; Schema: public; Owner: cats
--

CREATE TYPE public.gender AS ENUM (
    'male',
    'female',
    'unisex'
);


ALTER TYPE public.gender OWNER TO cats;

--
-- Name: validation_type; Type: TYPE; Schema: public; Owner: cats
--

CREATE TYPE public.validation_type AS ENUM (
    'search',
    'add'
);


ALTER TYPE public.validation_type OWNER TO cats;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cats; Type: TABLE; Schema: public; Owner: cats
--

CREATE TABLE public.cats (
    id integer NOT NULL,
    name text NOT NULL,
    description text,
    tags text[],
    gender public.gender NOT NULL,
    likes smallint DEFAULT 0 NOT NULL,
    dislikes smallint DEFAULT 0 NOT NULL,
    CONSTRAINT dislikes_positive CHECK ((dislikes >= 0)),
    CONSTRAINT likes_positive CHECK ((likes >= 0))
);


ALTER TABLE public.cats OWNER TO cats;

--
-- Name: cats_id_seq; Type: SEQUENCE; Schema: public; Owner: cats
--

CREATE SEQUENCE public.cats_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cats_id_seq OWNER TO cats;

--
-- Name: cats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cats
--

ALTER SEQUENCE public.cats_id_seq OWNED BY public.cats.id;


--
-- Name: cats_validations; Type: TABLE; Schema: public; Owner: cats
--

CREATE TABLE public.cats_validations (
    id integer NOT NULL,
    description text NOT NULL,
    regex text NOT NULL,
    type public.validation_type DEFAULT 'search'::public.validation_type NOT NULL
);


ALTER TABLE public.cats_validations OWNER TO cats;

--
-- Name: cats_validations_id_seq; Type: SEQUENCE; Schema: public; Owner: cats
--

CREATE SEQUENCE public.cats_validations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cats_validations_id_seq OWNER TO cats;

--
-- Name: cats_validations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cats
--

ALTER SEQUENCE public.cats_validations_id_seq OWNED BY public.cats_validations.id;


--
-- Name: images; Type: TABLE; Schema: public; Owner: cats
--

CREATE TABLE public.images (
    id integer NOT NULL,
    link character varying NOT NULL,
    id_cat integer NOT NULL
);


ALTER TABLE public.images OWNER TO cats;

--
-- Name: images_id_seq; Type: SEQUENCE; Schema: public; Owner: cats
--

CREATE SEQUENCE public.images_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.images_id_seq OWNER TO cats;

--
-- Name: images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cats
--

ALTER SEQUENCE public.images_id_seq OWNED BY public.images.id;


--
-- Name: cats id; Type: DEFAULT; Schema: public; Owner: cats
--

ALTER TABLE ONLY public.cats ALTER COLUMN id SET DEFAULT nextval('public.cats_id_seq'::regclass);


--
-- Name: cats_validations id; Type: DEFAULT; Schema: public; Owner: cats
--

ALTER TABLE ONLY public.cats_validations ALTER COLUMN id SET DEFAULT nextval('public.cats_validations_id_seq'::regclass);


--
-- Name: images id; Type: DEFAULT; Schema: public; Owner: cats
--

ALTER TABLE ONLY public.images ALTER COLUMN id SET DEFAULT nextval('public.images_id_seq'::regclass);


--
-- Data for Name: cats; Type: TABLE DATA; Schema: public; Owner: cats
--

COPY public.cats (id, name, description, tags, gender, likes, dislikes) FROM stdin;
1498	Кузя	\N	\N	unisex	0	0
1137	Аида	\N	\N	female	0	0
1120	Барсик	\N	\N	male	0	0
1115	Васька	\N	\N	unisex	0	0
1393	Жучка	\N	\N	unisex	0	0
1138	Аврора	\N	\N	female	0	0
1199	Багира	\N	\N	female	0	0
1474	Клякса	\N	\N	female	0	0
1558	Арсений	\N	\N	male	0	0
1295	Влада	\N	\N	female	0	0
1358	Дымка	\N	\N	unisex	0	0
1826	Абориген	\N	\N	male	0	0
1588	Балу	\N	\N	male	0	0
1836	Кекс	\N	\N	male	0	0
1871	Люся	\N	\N	female	0	0
1825	Максим	\N	\N	male	0	0
1832	Милка	\N	\N	female	0	0
1129	Абигель	\N	\N	female	0	0
2304	Соня	\N	\N	female	0	0
2242	Рыжик	\N	\N	male	0	0
3063	Салтыков-Щедрин	\N	\N	male	0	0
1533	Аксель	\N	\N	male	0	0
1333	Джесси	\N	\N	female	0	0
\.


--
-- Data for Name: cats_validations; Type: TABLE DATA; Schema: public; Owner: cats
--

COPY public.cats_validations (id, description, regex, type) FROM stdin;
20	Цифры не принимаются!	^\\D*$	search
21	Только имена на русском!	^[а-яА-ЯёЁ\\s-]*$	search
22	Из символов можно только "-" или пробел, единожды в середине имени или в конце	^([\\d\\wа-яА-ЯёЁ]+|[\\d\\wа-яА-ЯёЁ]+[-\\s]|[\\d\\wа-яА-ЯёЁ]+[-\\s][\\d\\wа-яА-ЯёЁ]+)$	search
23	Цифры не принимаются!	^\\D*$	add
25	Только имена на русском!	^[а-яА-ЯёЁ\\s-]*$	add
26	Имя слишком короткое!	^.{2,35}$	add
24	Из спецсимволов можно только тире или пробел единожды, только в середине имени	^([\\d\\wа-яА-ЯёЁ]+[-\\s]?[\\d\\wа-яА-ЯёЁ]+)$	add
\.


--
-- Data for Name: images; Type: TABLE DATA; Schema: public; Owner: cats
--

COPY public.images (id, link, id_cat) FROM stdin;
\.


--
-- Name: cats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cats
--

SELECT pg_catalog.setval('public.cats_id_seq', 3089, true);


--
-- Name: cats_validations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cats
--

SELECT pg_catalog.setval('public.cats_validations_id_seq', 26, true);


--
-- Name: images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cats
--

SELECT pg_catalog.setval('public.images_id_seq', 875, true);


--
-- Name: cats cats_pkey; Type: CONSTRAINT; Schema: public; Owner: cats
--

ALTER TABLE ONLY public.cats
    ADD CONSTRAINT cats_pkey PRIMARY KEY (id);


--
-- Name: cats_validations cats_validations_pkey; Type: CONSTRAINT; Schema: public; Owner: cats
--

ALTER TABLE ONLY public.cats_validations
    ADD CONSTRAINT cats_validations_pkey PRIMARY KEY (id);


--
-- Name: images images_pkey; Type: CONSTRAINT; Schema: public; Owner: cats
--

ALTER TABLE ONLY public.images
    ADD CONSTRAINT images_pkey PRIMARY KEY (id);


--
-- Name: cats unique_name; Type: CONSTRAINT; Schema: public; Owner: cats
--

ALTER TABLE ONLY public.cats
    ADD CONSTRAINT unique_name UNIQUE (name);


--
-- Name: images images_cats__fk; Type: FK CONSTRAINT; Schema: public; Owner: cats
--

ALTER TABLE ONLY public.images
    ADD CONSTRAINT images_cats__fk FOREIGN KEY (id_cat) REFERENCES public.cats(id) ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: cats
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;


--
-- PostgreSQL database dump complete
--


